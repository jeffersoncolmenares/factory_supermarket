require_relative './hall'
require_relative './shelf'
require_relative './product'

class FactorySuperMarket
  def self.go!
    products = create_products
        halls = create_halls
    create_super_market(halls)
  end

  def self.create_products 
    products = []
    products << Products.new(:name,:mark,:price)
     end


  def self.create_halls
    puts "Introduzca la cantidad de pasillos del super mercado"
    hall_count = gets.chomp.to_i

    halls = []
    hall_count.times do
    halls << Hall.new()
    end
    halls
  end

  def self.create_super_market (halls)
    SuperMarket.new (halls)
  end
end